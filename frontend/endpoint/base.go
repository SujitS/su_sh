package endpoint

import (
	"net/http"
)

type Endpoint func(http.ResponseWriter, *http.Request)

const emailREGEXP = `^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`
