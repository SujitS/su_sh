package model

import (
	"github.com/stretchr/testify/assert"
	"golang.org/x/crypto/bcrypt"
	"testing"

)

func TestHashPassword(t *testing.T) {
	password := "sekret@123"
	h, err := HashPassword(password)
	assert.Equal(t, nil, err)
	match := bcrypt.CompareHashAndPassword([]byte(h), []byte(password))
	assert.Equal(t, nil, match)
}

func TestMatchPassword(t *testing.T) {
	password := "sekret@123"
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)

	assert.Equal(t, nil, err)
	match := MatchPassword(password, string(bytes))
	assert.Equal(t, true, match)

	notmatch := MatchPassword("notmatch", string(bytes))
	assert.Equal(t, false, notmatch)
}

func TestGetHashedUser(t *testing.T) {
	c := Credential{
		Email: "test@tasty.com",
		Password: "sekret@123",
	}

	user, err := c.GetHashedUser()

	assert.Equal(t, nil, err)
	assert.Equal(t, "test@tasty.com", user.Email)

	match := MatchPassword(c.Password, user.HashPassword)
	assert.Equal(t, true, match)
}
