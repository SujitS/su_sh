package http

import (
	"bitbucket.org/SujitS/su_sh/api/endpoint"
	"bitbucket.org/SujitS/su_sh/api/service"

	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type APIServer struct {
	http    *http.Server
	service *service.APIService
}

func NewAPIServer(addr string, svc *service.APIService) *APIServer {
	r := mux.NewRouter()
	r.HandleFunc("/users", endpoint.CreateUsersPath(svc)).Methods("POST")
	r.HandleFunc("/login", endpoint.LoginPath(svc)).Methods("POST")
	r.HandleFunc("/users/gl_login", endpoint.GoogleLoginPath(svc)).Methods("GET")
	r.HandleFunc("/users/gl_callback", endpoint.GoogleCallbackPath(svc)).Methods("POST")
	r.HandleFunc("/logout", endpoint.LogoutPath(svc)).Methods("DELETE")
	r.HandleFunc("/users/profile", endpoint.UserProfilePath(svc)).Methods("GET", "POST")
	r.HandleFunc("/forget_password", endpoint.UserForgetPasswordPath(svc)).Methods("POST")
	r.HandleFunc("/reset_password", endpoint.UserResetPasswordPath(svc)).Methods("POST")

	return &APIServer{
		http: &http.Server{
			Addr:    addr,
			Handler: r,
		},
		service: svc,
	}
}

func (s *APIServer) ListenAndServe() error {
	log.Println("Server is running at port:", s.http.Addr)
	return s.http.ListenAndServe()
}
