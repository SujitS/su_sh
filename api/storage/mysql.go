package storage

import (
	"bitbucket.org/SujitS/su_sh/api/model"
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
)

type MysqlConfig struct {
	Name string
	Host string
	User string
	Pass string
	Port string
}

type MysqlStorage struct {
	DB *sql.DB
}

type StorageInterface interface {
	CreateUser(user *model.User) (*model.User, error)
	UserExists(user *model.User) bool
	FindUserByEmail(email string) (user *model.User, err error)
	FindUserAccess(email string, jwtAccessUid string) (*model.User, error)
	UpdateUserAccess(user *model.User, token *model.Token) (err error)
	UpdateUserProfile(currentUser *model.User, updatedUser *model.User) (*model.User, error)
	UpdateUserPassword(user *model.User, hashPassword string) error
	FindUserByResetPasswordToken(email string, token string) (*model.User, error)
	UpdateUserPasswordResetLink(user *model.User, rpl *model.ResetPasswordLink) (err error)
}

func NewMysqlStorage(config MysqlConfig) (*MysqlStorage, error) {
	connString := fmt.Sprintf("%s:%s@(%s:%s)/%s?parseTime=true", config.User, config.Pass, config.Host, config.Port, config.Name)
	log.Println("connecting to:", connString)

	db, err := sql.Open("mysql", connString)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return &MysqlStorage{
		DB: db,
	}, nil
}

func (mysql *MysqlStorage) CreateUser(user *model.User) (*model.User, error) {
	result, err := mysql.DB.Exec("INSERT INTO users (email, hash_password, created_at) VALUES (?, ?, ?)", user.Email, user.HashPassword, user.CreatedAt)
	if err != nil {
		return nil, err
	}

	l, _ := result.LastInsertId()
	log.Println("LastInsertId ", l)

	user.Id, err = result.LastInsertId()

	if err != nil {
		return nil, err
	}
	log.Println("LsastInsertId ", user.Id)
	return user, err
}

func (mysql *MysqlStorage) UserExists(user *model.User) bool {
	_, err := mysql.FindUserByEmail(user.Email)
	if err != nil {
		return false
	}

	return true
}

func (mysql *MysqlStorage) FindUserByEmail(email string) (*model.User, error) {
	var user model.User
	selectQuery := "SELECT id, email, COALESCE(name, ''), COALESCE(address, ''), COALESCE(telephone, ''), hash_password FROM users WHERE email = ?"
	row := mysql.DB.QueryRow(selectQuery, email)
	err := row.Scan(&user.Id, &user.Email, &user.Name, &user.Address, &user.Telephone, &user.HashPassword)

	return &user, err
}

func (mysql *MysqlStorage) FindUserAccess(email string, jwtAccessUid string) (*model.User, error) {
	var user model.User
	q := `SELECT id, email, COALESCE(name, ''), COALESCE(address, ''), COALESCE(telephone, ''), hash_password
				FROM users
				WHERE email = ? AND jwt_access_uuid = ?`

	row := mysql.DB.QueryRow(q, email, jwtAccessUid)
	err := row.Scan(&user.Id, &user.Email, &user.Name, &user.Address, &user.Telephone, &user.HashPassword)

	return &user, err
}

func (mysql *MysqlStorage) UpdateUserAccess(user *model.User, token *model.Token) (err error) {
	updateSQL := `
		UPDATE users
		SET jwt_access_uuid = ?
		WHERE email = ?;
	`
	_, err = mysql.DB.Exec(updateSQL, token.AccessUuid, user.Email)

	return
}

func (mysql *MysqlStorage) UpdateUserPasswordResetLink(user *model.User, rpl *model.ResetPasswordLink) (err error) {
	updateSQL := `
		UPDATE users
		SET jwt_reset_password_uuid = ?
		WHERE email = ?;
	`
	_, err = mysql.DB.Exec(updateSQL, rpl.TokenUuid, user.Email)

	return
}

func (mysql *MysqlStorage) FindUserByResetPasswordToken(email string, tokenUuid string) (*model.User, error) {
	var user model.User
	q := `SELECT id, email, COALESCE(name, ''), COALESCE(address, ''), COALESCE(telephone, ''), hash_password
				FROM users
				WHERE email = ? AND jwt_reset_password_uuid = ?`

	row := mysql.DB.QueryRow(q, email, tokenUuid)
	err := row.Scan(&user.Id, &user.Email, &user.Name, &user.Address, &user.Telephone, &user.HashPassword)

	return &user, err
}

func (mysql *MysqlStorage) UpdateUserProfile(currentUser *model.User, updatedUser *model.User) (*model.User, error) {
	updateSQL := `
		UPDATE users
		SET email = ?, name = ?, address = ?, telephone = ?
		WHERE email = ?;`

	_, err := mysql.DB.Exec(updateSQL, updatedUser.Email, updatedUser.Name, updatedUser.Address, updatedUser.Telephone, currentUser.Email)

	return currentUser, err
}

func (mysql *MysqlStorage) UpdateUserPassword(user *model.User, hashPassword string) error {
	updateSQL := `
		UPDATE users
		SET hash_password = ?
		WHERE email = ?;`

	_, err := mysql.DB.Exec(updateSQL, hashPassword, user.Email)

	return err
}
