package endpoint

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"

	"bitbucket.org/SujitS/su_sh/api/model"
	"bitbucket.org/SujitS/su_sh/api/service"
	"database/sql"
)

type Endpoint func(http.ResponseWriter, *http.Request)

func UserResetPasswordPath(svc *service.APIService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()

		var rpr *model.ResetPasswordRequest
		err := json.NewDecoder(r.Body).Decode(&rpr)
		if err != nil {
			log.Println(err.Error())
			respondError(w, err, http.StatusInternalServerError)
			return
		}

		log.Println("RPR", rpr)
		err = svc.ResetPassword(rpr)
		if err != nil {
			log.Println(err.Error())
			respondError(w, err, http.StatusInternalServerError)
			return
		}
		respondOk(w, nil)
	}
}

func UserForgetPasswordPath(svc *service.APIService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()

		var c *model.Credential
		err := json.NewDecoder(r.Body).Decode(&c)
		if err != nil {
			log.Println(err.Error())
			respondError(w, err, http.StatusInternalServerError)
			return
		}

		var user *model.User
		user, err = svc.Storage.FindUserByEmail(c.Email)
		if err == sql.ErrNoRows {
			log.Println("no email found")
			err = errors.New("Email doen't exists. Visit signup page to register")
			respondError(w, err, http.StatusUnprocessableEntity)
		} else if err != nil {
			log.Println(err.Error())
			respondError(w, err, http.StatusInternalServerError)
		} else {
			rpl, err := svc.ResetPasswordLink(user)
			if err != nil {
				respondError(w, err, http.StatusInternalServerError)
				return
			}

			respondOk(w, rpl)
		}
	}
}

func UserProfilePath(svc *service.APIService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUser, err := svc.Authenticate(r)
		if err != nil {
			log.Println(err.Error())
			respondError(w, err, http.StatusUnprocessableEntity)
			return
		}

		if r.Method != http.MethodPost {
			respondOk(w, currentUser)
			return
		}

		defer r.Body.Close()

		var updatedUser *model.User
		err = json.NewDecoder(r.Body).Decode(&updatedUser)
		if err != nil {
			log.Println(err.Error())
			respondError(w, err, http.StatusInternalServerError)
			return
		}

		if updatedUser.Email == "" {
			respondError(w, errors.New("Email can't be blank"), http.StatusUnprocessableEntity)
			return
		}

		currentUser, err = svc.Storage.UpdateUserProfile(currentUser, updatedUser)
		if err != nil {
			log.Println(err.Error())
			respondError(w, err, http.StatusInternalServerError)
			return
		}

		respondOk(w, currentUser)
	}
}

func LoginPath(svc *service.APIService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()

		var c *model.Credential
		err := json.NewDecoder(r.Body).Decode(&c)
		if err != nil {
			log.Println(err.Error())
			respondError(w, err, http.StatusInternalServerError)
			return
		}

		user, err := svc.Storage.FindUserByEmail(c.Email)
		if err != nil {
			log.Println(err.Error())
			respondError(w, err, http.StatusUnprocessableEntity)
			return
		}

		if !model.MatchPassword(c.Password, user.HashPassword) {
			err := errors.New("Password didn't match")
			respondError(w, err, http.StatusUnprocessableEntity)
			return
		}

		token, err := svc.Login(user)
		if err != nil {
			log.Println(err.Error())
			respondError(w, err, http.StatusInternalServerError)
			return
		}

		respondOk(w, token)
	}
}

func LogoutPath(svc *service.APIService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUser, err := svc.Authenticate(r)
		log.Println(currentUser)
		if err != nil {
			log.Println(err.Error())
			respondError(w, err, http.StatusUnprocessableEntity)
			return
		}

		if err = svc.Logout(currentUser); err != nil {
			log.Println(err.Error())
			respondError(w, err, http.StatusInternalServerError)
			return
		}

		respondOk(w, nil)
	}
}

func CreateUsersPath(svc *service.APIService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()

		var c *model.Credential
		err := json.NewDecoder(r.Body).Decode(&c)
		if err != nil {
			log.Println(err.Error())
			respondError(w, err, http.StatusInternalServerError)
			return
		}

		err = svc.ValidateCredential(c)
		if err != nil {
			respondError(w, err, http.StatusUnprocessableEntity)
			return
		}

		_, err = svc.Storage.FindUserByEmail(c.Email)
		if err == sql.ErrNoRows {
			user, err := c.GetHashedUser()
			if err != nil {
				log.Println(err.Error())
				respondError(w, err, http.StatusInternalServerError)
				return
			}

			user, err = svc.Storage.CreateUser(user)
			if err != nil {
				log.Println(err.Error())
				respondError(w, err, http.StatusInternalServerError)
				return
			}

			log.Println("User created successfully. Logging in", user.Id)

			token, err := svc.Login(user)
			if err != nil {
				log.Println(err.Error())
				respondError(w, err, http.StatusInternalServerError)
				return
			}

			respondOk(w, token)
		} else if err != nil {
			log.Println(err.Error())
			respondError(w, err, http.StatusInternalServerError)
		} else {
			err = errors.New("Email already exists. Visit signin page to SignIn")
			respondError(w, err, http.StatusUnprocessableEntity)
		}
	}
}

func GoogleLoginPath(svc *service.APIService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {
		respondOk(w, struct{ LoginUrl string }{svc.GoogleOAuth.LoginPageUrl()})
	}
}

func GoogleCallbackPath(svc *service.APIService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {

		log.Println(r.FormValue("state"))
		log.Println(r.FormValue("otp_code"))
		user, err := svc.GoogleOAuth.GetUserAndToken(r.FormValue("state"), r.FormValue("otp_code"))
		if err != nil {
			log.Println(err.Error())
			respondError(w, err, http.StatusInternalServerError)
			return
		}

		log.Println("User Exist?: ", svc.Storage.UserExists(user))
		if !svc.Storage.UserExists(user) {
			user, err = svc.Storage.CreateUser(user)
			if err != nil {
				log.Println(err.Error())
				respondError(w, err, http.StatusInternalServerError)
				return
			}
		} else {
			user, err = svc.Storage.FindUserByEmail(user.Email)
			if err != nil {
				log.Println(err.Error())
				respondError(w, err, http.StatusInternalServerError)
				return
			}
		}

		token, err := svc.Login(user)
		if err != nil {
			log.Println(err.Error())
			respondError(w, err, http.StatusInternalServerError)
			return
		}

		respondOk(w, token)
	}
}

func respondError(w http.ResponseWriter, err error, status int) {
	log.Println(err)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	enc := json.NewEncoder(w)
	enc.Encode(map[string]string{"error": err.Error()})
}

func respondOk(w http.ResponseWriter, body interface{}) {
	w.Header().Set("Content-Type", "application/json")

	if body == nil {
		w.WriteHeader(http.StatusNoContent)
	} else {
		w.WriteHeader(http.StatusOK)
		enc := json.NewEncoder(w)
		enc.Encode(body)
	}
}
