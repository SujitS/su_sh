package oauth

import (
	"bitbucket.org/SujitS/su_sh/api/model"

	"encoding/json"
	"errors"
	"fmt"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"net/http"
	"net/url"
	"time"
)

type OAuthInterface interface {
	LoginPageUrl() string
	GetUserAndToken(state string, otp_code string) (*model.User, error)
}

type GoogleOAuth struct {
	Config *oauth2.Config
}

const (
	oauthState = "arsenic_123"
)

func NewGoogleOAuth(client_id string, client_secret string, redirectUrl string) *GoogleOAuth {
	return &GoogleOAuth{
		Config: &oauth2.Config{
			ClientID:     client_id,
			ClientSecret: client_secret,
			Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
			Endpoint:     google.Endpoint,
			RedirectURL:  redirectUrl,
		},
	}
}

func (g *GoogleOAuth) LoginPageUrl() (loginUrl string) {
	return g.Config.AuthCodeURL(oauthState)
}

func (g *GoogleOAuth) GetUserAndToken(state string, otp_code string) (*model.User, error) {
	if state != oauthState {
		err := errors.New("Invalid OAuth State")
		return nil, err
	}

	if otp_code == "" {
		err := errors.New("Code Not Found")
		return nil, err
	}

	token, err := g.Config.Exchange(oauth2.NoContext, otp_code)
	if err != nil {
		err = errors.New(fmt.Sprintf("OAuth Exchange Failed with: %s", err.Error()))
		return nil, err
	}

	p, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + url.QueryEscape(token.AccessToken))
	if err != nil {
		return nil, err
	}
	defer p.Body.Close()

	var profile map[string]interface{}
	if err = json.NewDecoder(p.Body).Decode(&profile); err != nil {
		return nil, err
	}

	user := &model.User{
		Email:     fmt.Sprintf("%v", profile["email"]),
		CreatedAt: time.Now(),
	}

	return user, err
}
