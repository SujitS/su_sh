package http

import (
	"bitbucket.org/SujitS/su_sh/frontend/endpoint"
	"bitbucket.org/SujitS/su_sh/frontend/service"

	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type FrontendServer struct {
	http *http.Server
}

func NewFrontendServer(addr string, auth *service.AuthService) *FrontendServer {
	r := mux.NewRouter()
	r.HandleFunc("/signin", endpoint.UserLoginPath(auth)).Methods("GET", "POST")
	r.HandleFunc("/signup", endpoint.UserRegistrationPath(auth)).Methods("GET", "POST")
	r.HandleFunc("/login-gl", endpoint.GoogleLoginPath(auth)).Methods("GET")
	r.HandleFunc("/callback-gl", endpoint.GoogleCallbackPath(auth)).Methods("GET")
	r.HandleFunc("/", endpoint.UserProfilePath(auth)).Methods("GET")
	r.HandleFunc("/profile/edit", endpoint.UserProfileEditPath(auth)).Methods("GET", "POST")
	r.HandleFunc("/signout", endpoint.UserLogoutPath(auth)).Methods("GET")
	r.HandleFunc("/forget-password", endpoint.UserForgetPasswordPath(auth)).Methods("GET", "POST")
	r.HandleFunc("/reset-password", endpoint.UserResetPasswordPath(auth)).Methods("GET", "POST")

	return &FrontendServer{
		http: &http.Server{
			Addr:    addr,
			Handler: r,
		},
	}
}

func (s *FrontendServer) ListenAndServe() error {
	log.Println("Server is running at port:", s.http.Addr)
	return s.http.ListenAndServe()
}
