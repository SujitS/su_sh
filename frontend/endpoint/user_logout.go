package endpoint

import (
	"bitbucket.org/SujitS/su_sh/frontend/service"

	"fmt"
	"log"
	"net/http"
	"net/url"
)

func UserLogoutPath(auth *service.AuthService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {
		apiURL, _ := url.Parse(fmt.Sprintf("%s/logout", auth.ApiURL))

		req, err := http.NewRequest("DELETE", apiURL.String(), nil)
		req.Header.Add("jwt-access-token", auth.AccessToken(r))

		client := &http.Client{}
		resp, err := client.Do(req)

		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		if resp.StatusCode != http.StatusOK {
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}

		if err = auth.Logout(w, r); err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
	}
}
