package service

import (
	"bitbucket.org/SujitS/su_sh/api/model"
	"bitbucket.org/SujitS/su_sh/api/oauth"
	"bitbucket.org/SujitS/su_sh/api/storage"

	"errors"
	"fmt"
	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"log"
	"net/http"
	"regexp"
	"time"
)

type APIService struct {
	Storage        storage.StorageInterface
	GoogleOAuth    oauth.OAuthInterface
	JwtSecretToken string
}

const emailREGEXP = `^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`

func (api *APIService) ValidateCredential(c *model.Credential) (errs error) {
	re := regexp.MustCompile(emailREGEXP)

	if !re.MatchString(c.Email) {
		return errors.New("Invalid Email Address")
	}

	if c.PasswordConfirmation != c.Password {
		return errors.New("Password confirmation doesn't match Password")
	}

	if len(c.Password) < 8 {
		return errors.New("Password is too short (minimum is 8 characters)")
	}
	return
}

func (api *APIService) Login(user *model.User) (*model.Token, error) {
	token, err := api.generateAccessToken(user.Email)
	if err != nil {
		return nil, err
	}

	err = api.Storage.UpdateUserAccess(user, token)
	if err != nil {
		return nil, err
	}
	return token, err
}

func (api *APIService) Logout(user *model.User) error {
	err := api.Storage.UpdateUserAccess(user, new(model.Token))
	return err
}

func (api *APIService) Authenticate(r *http.Request) (*model.User, error) {
	jwtAccessToken := r.Header.Get("jwt-access-token")

	token, err := jwt.ParseWithClaims(jwtAccessToken, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(api.JwtSecretToken), nil
	})

	if err != nil {
		return nil, err
	}

	if claims, ok := token.Claims.(*jwt.StandardClaims); ok && token.Valid {
		return api.Storage.FindUserAccess(fmt.Sprintf("%v", claims.Issuer), fmt.Sprintf("%v", claims.Id))
	} else {
		return nil, err
	}
}

func (api *APIService) ResetPasswordLink(user *model.User) (rpl *model.ResetPasswordLink, err error) {
	rpl = new(model.ResetPasswordLink)
	rpl.TokenUuid = uuid.New().String()
	claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		Id:        rpl.TokenUuid,
		Issuer:    user.Email,
		ExpiresAt: time.Now().Add(time.Hour * 24 * 7).Unix(),
	})
	rpl.Token, err = claims.SignedString([]byte(api.JwtSecretToken))
	rpl.Url = "/reset_password"

	err = api.Storage.UpdateUserPasswordResetLink(user, rpl)
	return
}

func (api *APIService) ResetPassword(r *model.ResetPasswordRequest) error {
	token, err := jwt.ParseWithClaims(r.Token, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(api.JwtSecretToken), nil
	})

	claims, ok := token.Claims.(*jwt.StandardClaims)
	if !ok || !token.Valid {
		return err
	}

	user, err := api.Storage.FindUserByResetPasswordToken(fmt.Sprintf("%v", claims.Issuer), fmt.Sprintf("%v", claims.Id))
	if err != nil {
		return err
	}

	var h string
	h, err = model.HashPassword(r.Password)
	if err != nil {
		return err
	}
	log.Println("hasedPassword", h)

	err = api.Storage.UpdateUserPassword(user, h)
	return err
}

func (api *APIService) generateAccessToken(email string) (token *model.Token, err error) {
	token = new(model.Token)
	token.UserEmail = email
	token.AtExpires = time.Now().Add(time.Minute * 15).Unix()
	token.AccessUuid = uuid.New().String()

	claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		Id:        token.AccessUuid,
		Issuer:    email,
		ExpiresAt: token.AtExpires,
	})

	token.AccessToken, err = claims.SignedString([]byte(api.JwtSecretToken))

	return
}
