package endpoint

import (
	"bitbucket.org/SujitS/su_sh/frontend/service"
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
)

type ForgetPasswordForm struct {
	Email string `json:"email"`
	Error string
}

type ResetPasswordForm struct {
	Token    string `json:"token"`
	Password string `json:"password"`
	Error    string
}

func (form *ForgetPasswordForm) submit(apiUrl string) (*http.Response, error) {
	apiURL, _ := url.Parse(fmt.Sprintf("%s/forget_password", apiUrl))
	params, err := json.Marshal(&form)
	if err != nil {
		return nil, err
	}

	return http.Post(apiURL.String(), "application/json", bytes.NewBuffer(params))
}

func (form *ResetPasswordForm) submit(apiUrl string) (*http.Response, error) {
	apiURL, _ := url.Parse(fmt.Sprintf("%s/reset_password", apiUrl))
	params, err := json.Marshal(&form)
	if err != nil {
		return nil, err
	}

	return http.Post(apiURL.String(), "application/json", bytes.NewBuffer(params))
}

func UserResetPasswordPath(auth *service.AuthService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {
		var rpf ResetPasswordForm
		rpf.Token = r.URL.Query().Get("reset_password_token")
		cwd, _ := os.Getwd()
		html := filepath.Join(cwd, "./views/reset_password.html")
		layout := filepath.Join(cwd, "./views/application.layout.html")

		tmpl, err := template.ParseFiles(html, layout)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", 500)
			return
		}

		if r.Method != http.MethodPost {
			err = tmpl.Execute(w, rpf)
			if err != nil {
				log.Println(err.Error())
				http.Error(w, "Internal Server Error", 500)
			}

			return
		}

		form := &ResetPasswordForm{
			Token:    r.FormValue("reset_password_token"),
			Password: r.FormValue("password"),
		}
		resp, err := form.submit(auth.ApiURL)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		defer resp.Body.Close()

		if resp.StatusCode != http.StatusNoContent {
			var response map[string]interface{}
			if err = json.NewDecoder(resp.Body).Decode(&response); err != nil {
				log.Println(err.Error())
				http.Error(w, "Internal Server Error", http.StatusInternalServerError)
				return
			}

			form.Error = fmt.Sprintf("%v", response["error"])
			tmpl.Execute(w, form)

			return
		}

		http.Redirect(w, r, "/signin", http.StatusSeeOther)
	}
}

func UserForgetPasswordPath(auth *service.AuthService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {
		cwd, _ := os.Getwd()
		html := filepath.Join(cwd, "./views/forget_password.html")
		layout := filepath.Join(cwd, "./views/application.layout.html")

		tmpl, err := template.ParseFiles(html, layout)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", 500)
			return
		}

		if r.Method != http.MethodPost {
			err = tmpl.Execute(w, nil)
			if err != nil {
				log.Println(err.Error())
				http.Error(w, "Internal Server Error", 500)
			}

			return
		}

		form := &ForgetPasswordForm{
			Email: r.FormValue("email"),
		}

		resp, err := form.submit(auth.ApiURL)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		defer resp.Body.Close()

		var response map[string]interface{}
		if err = json.NewDecoder(resp.Body).Decode(&response); err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		if resp.StatusCode != http.StatusOK {
			form.Error = fmt.Sprintf("%v", response["error"])
			tmpl.Execute(w, form)

			return
		}
		ru := fmt.Sprintf("/reset-password?reset_password_token=%v", response["token"])
		http.Redirect(w, r, ru, http.StatusSeeOther)
	}
}
