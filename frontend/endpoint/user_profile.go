package endpoint

import (
	"bitbucket.org/SujitS/su_sh/frontend/service"
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
)

type UserProfile struct {
	Email     string `json:"email"`
	Name      string `json:"name"`
	Address   string `json:"address"`
	Telephone string `json:"telephone"`
	Error     string
}

func UserProfilePath(auth *service.AuthService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {
		apiURL, _ := url.Parse(fmt.Sprintf("%s/users/profile", auth.ApiURL))

		req, err := http.NewRequest("GET", apiURL.String(), nil)
		req.Header.Add("jwt-access-token", auth.AccessToken(r))

		client := &http.Client{}
		resp, err := client.Do(req)

		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		if resp.StatusCode != http.StatusOK {
			http.Redirect(w, r, "/signin", http.StatusTemporaryRedirect)
			return
		}

		var profile UserProfile
		err = json.NewDecoder(resp.Body).Decode(&profile)
		if err != nil {
			log.Println(err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		cwd, _ := os.Getwd()
		html := filepath.Join(cwd, "./views/profile.html")
		layout := filepath.Join(cwd, "./views/application.layout.html")

		tmpl, err := template.ParseFiles(html, layout)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", 500)
			return
		}

		tmpl.Execute(w, profile)
	}
}

func UserProfileEditPath(auth *service.AuthService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {
		cwd, _ := os.Getwd()
		html := filepath.Join(cwd, "./views/edit_profile.html")
		layout := filepath.Join(cwd, "./views/application.layout.html")

		tmpl, err := template.ParseFiles(html, layout)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", 500)
			return
		}

		apiURL, _ := url.Parse(fmt.Sprintf("%s/users/profile", auth.ApiURL))

		req, err := http.NewRequest("GET", apiURL.String(), nil)
		req.Header.Add("jwt-access-token", auth.AccessToken(r))

		client := &http.Client{}
		resp, err := client.Do(req)

		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		if resp.StatusCode != http.StatusOK {
			http.Redirect(w, r, "/signin", http.StatusTemporaryRedirect)
			return
		}

		var currentProfile UserProfile
		err = json.NewDecoder(resp.Body).Decode(&currentProfile)
		if err != nil {
			log.Println(err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		if r.Method != http.MethodPost {
			err = tmpl.Execute(w, currentProfile)
			if err != nil {
				log.Println(err.Error())
				http.Error(w, "Internal Server Error", 500)
			}

			return
		}

		updatedProfile := &UserProfile{
			Email:     r.FormValue("email"),
			Name:      r.FormValue("name"),
			Address:   r.FormValue("address"),
			Telephone: r.FormValue("telephone"),
		}

		resp, err = currentProfile.update(auth.ApiURL, updatedProfile, auth.AccessToken(r))
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		defer resp.Body.Close()

		var response map[string]interface{}
		if err = json.NewDecoder(resp.Body).Decode(&response); err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		if resp.StatusCode != http.StatusOK {
			updatedProfile.Error = fmt.Sprintf("%v", response["error"])
			tmpl.Execute(w, updatedProfile)

			return
		}

		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
}

func (c *UserProfile) update(apiUrl string, u *UserProfile, accessToken string) (*http.Response, error) {
	apiURL, _ := url.Parse(fmt.Sprintf("%s/users/profile", apiUrl))
	params, err := json.Marshal(&u)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", apiURL.String(), bytes.NewBuffer(params))
	req.Header.Add("jwt-access-token", accessToken)

	client := &http.Client{}

	return client.Do(req)
}
