package service

import (
	"fmt"
	"github.com/gorilla/sessions"
	"net/http"
)

type AuthService struct {
	ApiURL string
	Store  *sessions.CookieStore
}

func NewAuthService(apiUrl string, sessionKey string) *AuthService {
	auth := new(AuthService)
	auth.ApiURL = apiUrl
	auth.Store = sessions.NewCookieStore([]byte(sessionKey))

	return auth
}

func (auth *AuthService) Login(w http.ResponseWriter, r *http.Request, p map[string]interface{}) error {
	ls, _ := auth.Store.Get(r, "login")
	ls.Values["accessToken"] = p["access_token"]

	return ls.Save(r, w)
}

func (auth *AuthService) Logout(w http.ResponseWriter, r *http.Request) error {
	ls, _ := auth.Store.Get(r, "login")
	ls.Values["accessToken"] = nil

	return ls.Save(r, w)
}

func (auth *AuthService) AccessToken(r *http.Request) string {
	ls, _ := auth.Store.Get(r, "login")
	return fmt.Sprintf("%v", ls.Values["accessToken"])
}
