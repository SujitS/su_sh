package main

import (
	"bitbucket.org/SujitS/su_sh/frontend/http"
	"bitbucket.org/SujitS/su_sh/frontend/service"

	"github.com/joho/godotenv"
	"log"
	"os"
)

func main() {
	godotenv.Load()
	auth := service.NewAuthService(os.Getenv("APIURL"), os.Getenv("SESSIONKEY"))
	server := http.NewFrontendServer(os.Getenv("FRONTEND_PORT"), auth)
	log.Fatal(server.ListenAndServe())
}
