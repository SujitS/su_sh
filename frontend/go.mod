module bitbucket.org/SujitS/su_sh/frontend

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1
	github.com/joho/godotenv v1.4.0
)
