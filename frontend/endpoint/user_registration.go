package endpoint

import (
	"bitbucket.org/SujitS/su_sh/frontend/service"

	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
)

type RegistrationForm struct {
	Email                string `json:"email"`
	Password             string `json:"password"`
	PasswordConfirmation string `json:"password_confirmation"`
	Errors               []string
}

func (form *RegistrationForm) validate() {
	re := regexp.MustCompile(emailREGEXP)
	if !re.MatchString(form.Email) {
		form.Errors = append(form.Errors, "Invalid Email Address")
	}

	if form.PasswordConfirmation != form.Password {
		form.Errors = append(form.Errors, "Password confirmation doesn't match Password")
	}

	if len(form.Password) < 8 {
		form.Errors = append(form.Errors, "Password is too short (minimum is 8 characters)")
	}
	return
}

func (form *RegistrationForm) submit(apiUrl string) (*http.Response, error) {
	apiURL, _ := url.Parse(fmt.Sprintf("%s/users", apiUrl))
	params, err := json.Marshal(&form)
	if err != nil {
		return nil, err
	}

	return http.Post(apiURL.String(), "application/json", bytes.NewBuffer(params))
}

func UserRegistrationPath(auth *service.AuthService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {
		cwd, _ := os.Getwd()
		html := filepath.Join(cwd, "./views/signup.html")
		layout := filepath.Join(cwd, "./views/application.layout.html")

		tmpl, err := template.ParseFiles(html, layout)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", 500)
			return
		}

		if r.Method != http.MethodPost {
			err = tmpl.Execute(w, nil)
			if err != nil {
				log.Println(err.Error())
				http.Error(w, "Internal Server Error", 500)
			}

			return
		}

		form := &RegistrationForm{
			Email:                r.FormValue("email"),
			Password:             r.FormValue("password"),
			PasswordConfirmation: r.FormValue("password_confirmation"),
		}

		form.validate()
		if len(form.Errors) > 0 {
			tmpl.Execute(w, form)
			return
		}

		resp, err := form.submit(auth.ApiURL)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		defer resp.Body.Close()

		log.Println(resp.Body)
		var response map[string]interface{}
		if err = json.NewDecoder(resp.Body).Decode(&response); err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		log.Println("response ", response)

		if resp.StatusCode != http.StatusOK {
			form.Errors = []string{fmt.Sprintf("%v", response["error"])}
			tmpl.Execute(w, form)

			return
		}

		if err = auth.Login(w, r, response); err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
}
