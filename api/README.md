## DB SETUP
`CREATE TABLE users (
id INT AUTO_INCREMENT,
email varchar(255) NOT NULL UNIQUE,
name TEXT,
address TEXT,
telephone TEXT,
hash_password TEXT,
jwt_reset_password_uuid TEXT,
jwt_access_uuid TEXT,
created_at DATETIME,
PRIMARY KEY (id)
);`
---
## ENV VARIABLE SETUP (DEV)
>$ cp env.example .env

## RUN APP
>$ go run main.go
