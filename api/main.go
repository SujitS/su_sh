package main

import (
	"bitbucket.org/SujitS/su_sh/api/http"
	"bitbucket.org/SujitS/su_sh/api/oauth"
	"bitbucket.org/SujitS/su_sh/api/service"
	"bitbucket.org/SujitS/su_sh/api/storage"

	"github.com/joho/godotenv"
	"log"
	"os"
)

const (
	envDBName = "DB_NAME"
	envDBHost = "DB_HOST"
	envDBUser = "DB_USER"
	envDBPass = "DB_PASSWORD"
	envDBPort = "DB_PORT"

	envAPIPort = "API_PORT"

	envGoogleClientID     = "GOOGLE_CLIENT_ID"
	envGoogleClientSecret = "GOOGLE_CLIENT_SECRET"
	envGoogleRedirectUrl  = "GOOGLE_REDIRECT_URL"

	envJwtSecretToken = "JWT_SECRET_TOKEN"
)

type config struct {
	mysqlConfig        storage.MysqlConfig
	googleClientId     string
	googleClientSecret string
	googleRedirectUtl  string
	jwtSecretToken     string
	apiPort            string
}

func main() {
	config := loadConfig()

	mysql, err := storage.NewMysqlStorage(config.mysqlConfig)
	if err != nil {
		log.Fatal(err)
	}

	svc := &service.APIService{
		Storage:        mysql,
		GoogleOAuth:    oauth.NewGoogleOAuth(config.googleClientId, config.googleClientSecret, config.googleRedirectUtl),
		JwtSecretToken: config.jwtSecretToken,
	}

	server := http.NewAPIServer(config.apiPort, svc)
	log.Fatal(server.ListenAndServe())
}

func loadConfig() config {
	godotenv.Load()
	mysqlConfig := storage.MysqlConfig{
		Name: os.Getenv(envDBName),
		Host: os.Getenv(envDBHost),
		User: os.Getenv(envDBUser),
		Pass: os.Getenv(envDBPass),
		Port: os.Getenv(envDBPort),
	}

	return config{
		mysqlConfig:        mysqlConfig,
		googleClientId:     os.Getenv(envGoogleClientID),
		googleClientSecret: os.Getenv(envGoogleClientSecret),
		googleRedirectUtl:  os.Getenv(envGoogleRedirectUrl),
		jwtSecretToken:     os.Getenv(envJwtSecretToken),
		apiPort: os.Getenv(envAPIPort),
	}
}
