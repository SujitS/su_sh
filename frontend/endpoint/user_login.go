package endpoint

import (
	"bitbucket.org/SujitS/su_sh/frontend/service"

	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
)

type LoginForm struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	Error    string
}

func (form *LoginForm) submit(apiUrl string) (*http.Response, error) {
	apiURL, _ := url.Parse(fmt.Sprintf("%s/login", apiUrl))
	params, err := json.Marshal(&form)
	if err != nil {
		return nil, err
	}

	return http.Post(apiURL.String(), "application/json", bytes.NewBuffer(params))
}

func UserLoginPath(auth *service.AuthService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {
		cwd, _ := os.Getwd()
		html := filepath.Join(cwd, "./views/signin.html")
		layout := filepath.Join(cwd, "./views/application.layout.html")

		tmpl, err := template.ParseFiles(html, layout)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", 500)
			return
		}

		if r.Method != http.MethodPost {
			err = tmpl.Execute(w, nil)
			if err != nil {
				log.Println(err.Error())
				http.Error(w, "Internal Server Error", 500)
			}

			return
		}

		form := &LoginForm{
			Email:    r.FormValue("email"),
			Password: r.FormValue("password"),
		}

		resp, err := form.submit(auth.ApiURL)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		defer resp.Body.Close()

		var response map[string]interface{}
		if err = json.NewDecoder(resp.Body).Decode(&response); err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		if resp.StatusCode != http.StatusOK {
			form.Error = fmt.Sprintf("%v", response["error"])
			tmpl.Execute(w, form)

			return
		}

		if err = auth.Login(w, r, response); err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
}
