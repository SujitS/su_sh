package model

import (
	"golang.org/x/crypto/bcrypt"
	"time"
)

type Credential struct {
	Email                string `json:"email"`
	Password             string `json:"password"`
	PasswordConfirmation string `json:"password_confirmation"`
}

type ResetPasswordLink struct {
	TokenUuid string `json:"-"`
	Token     string `json:"token"`
	Url       string `json:"url"`
}

type ResetPasswordRequest struct {
	Token    string `json:"token"`
	Password string `json:"password"`
}

type User struct {
	Id                 int64     `json:"id"`
	Email              string    `json:"email"`
	Name               string    `json:"name"`
	Address            string    `json:"address"`
	Telephone          string    `json:"telephone"`
	HashPassword       string    `json:"-"`
	CreatedAt          time.Time `json:"created_at"`
	ResetPasswordToken string    `json:"-"`
	JwtAccessUuid      string    `json:"-"`
}

type Token struct {
	Id          int64  `json:"id"`
	UserEmail   string `json:"user_email"`
	AccessToken string `json:"access_token"`
	AccessUuid  string `json:"-"`
	AtExpires   int64  `json:"-"`
}

func (c *Credential) GetHashedUser() (*User, error) {
	hash, err := HashPassword(c.Password)
	if err != nil {
		return nil, err
	}

	return &User{
		Email:        c.Email,
		CreatedAt:    time.Now(),
		HashPassword: hash,
	}, nil
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}
func MatchPassword(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
