package endpoint

import (
	"bitbucket.org/SujitS/su_sh/frontend/service"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
)

func GoogleLoginPath(auth *service.AuthService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {
		apiURL, _ := url.Parse(fmt.Sprintf("%s/users/gl_login", auth.ApiURL))
		resp, err := http.Get(apiURL.String())
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		if resp.StatusCode != http.StatusOK {
			http.Redirect(w, r, "/signin", http.StatusTemporaryRedirect)
			return
		}

		var apiResponse map[string]string
		err = json.NewDecoder(resp.Body).Decode(&apiResponse)
		if err != nil {
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, apiResponse["LoginUrl"], http.StatusTemporaryRedirect)
	}
}

func GoogleCallbackPath(auth *service.AuthService) Endpoint {
	return func(w http.ResponseWriter, r *http.Request) {
		apiURL, _ := url.Parse(fmt.Sprintf("%s/users/gl_callback", auth.ApiURL))
		params := url.Values{}
		params.Add("state", r.URL.Query().Get("state"))
		params.Add("otp_code", r.URL.Query().Get("code"))

		resp, err := http.PostForm(apiURL.String(), params)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		if resp.StatusCode != http.StatusOK {
			http.Redirect(w, r, "/signin", http.StatusTemporaryRedirect)
			return
		}

		var apiResponse map[string]interface{}
		if err = json.NewDecoder(resp.Body).Decode(&apiResponse); err != nil {
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		if err = auth.Login(w, r, apiResponse); err != nil {
			log.Println(err.Error())
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
	}
}
